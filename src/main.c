#include <stdio.h>

#include "test.h"

int main() {
    bool res = test_all();
    if (!res) {
        printf("TESTS FAILED\n");
        return 1;
    }

    printf("TESTS PASSED\n");
    return 0;
}
